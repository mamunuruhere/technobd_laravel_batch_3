@include('layouts.partials.header')

<div class="container">

    @include('layouts.partials.nav')

    @include('layouts.elements.breadcrumb')

    <h1>@yield('page_header')</h1>

    @yield('content')

</div>

@include('layouts.partials.footer')