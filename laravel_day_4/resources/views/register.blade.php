
@extends('layouts.master')

@section('title', 'Students')

@section('page_header', "Student Registration Form")

@section('breadcrumb', "Students / Register")


@section('content')
    <a href="{{ url('/students') }}" class="btn btn-success">Students</a>
    <hr>
    {{--<form action="" method="">--}}
    {!! Form::open(['url' => '', 'method' => 'post']) !!}

        <div class="form-group">
            {{--<label for='name'>Name</label>--}}
            {!!  Form::label('nameField', 'Name') !!}
            {{--<input type="text" name="name" id="name" value='' class="form-control">--}}
            {!!  Form::text('name', null, ['class' => 'form-control', 'id'=>'nameField', 'placeholder'=>'Enter Your Name']) !!}
        </div>

        <div class="form-group">
            {{--<label for="email">Email</label>--}}
            {!!  Form::label('email', 'Email') !!}
            {{--<input type="email" name="email" value="" id="" class="form-control" placeholder="Enter Email">--}}
            {!!  Form::email('email', null, ['class' => 'form-control', 'id'=>'email', 'placeholder'=>'Enter Email']) !!}

        </div>

        <div class="form-group">
            {{--<button type="submit" class="btn btn-danger">Register</button>--}}
            {!!  Form::submit('Register', ['class' => 'btn btn-danger']) !!}
        </div>

    {{--</form>--}}
    {!! Form::close() !!}
@endsection

@push('css')

<style>
    .container{
        background: cadetblue;
    }
</style>

@endpush