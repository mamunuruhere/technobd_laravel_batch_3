@extends('layouts.master')

@section('title', 'Teachers')

@section('page_header', "Teachers's List")

@section('breadcrumb', "Teachers")

@section('content')

    <h1>Teachers</h1>

@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('front-end/css/style.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('front-end/js/script.js') }}"></script>
@endpush