@extends('admin.layouts.master')

@section('page_title', 'Categories')

@section('content')
        <!-- /.row -->
<a href="{{ route('categories.index') }}" class="btn btn-sm btn-success">Categories</a>
<hr>
{{--<form action="" method="">--}}
{!! Form::open(['url' => 'admin/categories/1', 'method' => 'put']) !!}

<div class="form-group">
    {{--<label for='name'>Name</label>--}}
    {!!  Form::label('title', 'Title') !!}
    {{--<input type="text" name="name" id="name" value='' class="form-control">--}}
    {!!  Form::text('title', null, ['class' => 'form-control', 'id'=>'title', 'placeholder'=>'Enter Category Title']) !!}
</div>


<div class="form-group">
    {{--<button type="submit" class="btn btn-danger">Register</button>--}}
    {!!  Form::submit('Update', ['class' => 'btn btn-danger']) !!}
</div>

{{--</form>--}}
{!! Form::close() !!}
        <!-- /.row -->
@endsection